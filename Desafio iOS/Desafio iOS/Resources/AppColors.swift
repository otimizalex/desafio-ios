//
//  ColorStrings.swift
//  Desafio iOS
//
//  Created by Alex Freitas on 29/11/21.
//

import UIKit

struct AppColors {
    static let loginButtonInactive = UIColor(named: "neutral3")
    static let loginButtonActive = UIColor(named: "Primary")
    static let textFieldLabelGray = UIColor(named: "neutral4")
    static let textFieldWarningRed = UIColor(named: "red-warning")
}
