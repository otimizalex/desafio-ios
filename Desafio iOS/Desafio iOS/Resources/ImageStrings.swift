//
//  ImageStrings.swift
//  Desafio iOS
//
//  Created by Alex Freitas on 22/11/21.
//

import Foundation

struct ImageStrings {
    // MARK: Visual Identity
    static let ioasysLogo = "ioasys-logo"

    // MARK: UI
    static let backgroundLaunch = "background-1"
    static let backgroundLogin = "background-2"

    // MARK: States
    static let notFound = "not-found"
}
