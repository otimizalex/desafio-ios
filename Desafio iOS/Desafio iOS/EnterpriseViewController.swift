//
//  CompanyViewController.swift
//  Desafio iOS
//
//  Created by Alex Freitas on 23/11/21.
//

import UIKit

class EnterpriseViewController: UIViewController {

    @IBOutlet weak var enterpriseImage: CustomImageView!
    @IBOutlet weak var enterpriseDescriptionText: UITextView!

    var enterprise: Enterprise?

    override func viewDidLoad() {
        setupNavBar()
        setupViewInformation()
    }

    private func setupViewInformation() {
        if let enterprise = enterprise,
           let imagePath = enterprise.photo {
            enterpriseDescriptionText.text = enterprise.description
            let imageUrl = Helpers.getImageURL(path: imagePath)
            enterpriseImage.downloadImage(from: imageUrl)
        }

        enterpriseDescriptionText.isEditable = false
    }

    private func setupNavBar() {
        let title = UILabel(frame: CGRect(x: 0, y: 0, width: 60, height: 30))
        title.text = enterprise?.name

        navigationItem.title = enterprise?.type.name
        navigationItem.titleView = title
    }
}
