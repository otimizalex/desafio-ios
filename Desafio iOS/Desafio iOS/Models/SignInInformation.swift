//
//  SignInInformation.swift
//  Desafio iOS
//
//  Created by Alex Freitas on 24/11/21.
//

import Foundation

struct SignInInformation: Codable {
    var email: String
    var password: String
}
