//
//  Investor.swift
//  Desafio iOS
//
//  Created by Alex Freitas on 24/11/21.
//

import Foundation

struct InvestorResponse: Decodable {
    var investor: Investor
}

struct Investor: Decodable {
    var id: Int
    var name: String

    enum CodingKeys: String, CodingKey {
        case id
        case name = "investor_name"
    }
}
