//
//  Enterprise.swift
//  Desafio iOS
//
//  Created by Alex Freitas on 24/11/21.
//

import Foundation

struct EnterpriseListResponse: Decodable {
    var list: [Enterprise]

    enum CodingKeys: String, CodingKey {
        case list = "enterprises"
    }
}

struct Enterprise: Decodable {
    var id: Int
    var email: String?
    var facebook: String?
    var twitter: String?
    var linkedin: String?
    var phone: String?
    var ownEnterprise: Bool
    var name: String?
    var photo: String?
    var description: String?
    var city: String?
    var country: String?
    var value: Int
    var sharePrice: Double
    var type: EnterpriseType

    enum CodingKeys: String, CodingKey {
        case id
        case email = "email_enterprise"
        case facebook
        case twitter
        case linkedin
        case phone
        case ownEnterprise = "own_enterprise"
        case name = "enterprise_name"
        case photo
        case description
        case city
        case country
        case value
        case sharePrice = "share_price"
        case type = "enterprise_type"
    }
}

struct EnterpriseType: Decodable {
    var id: Int
    var name: String?

    enum CodingKeys: String, CodingKey {
        case id
        case name = "enterprise_type_name"
    }
}
