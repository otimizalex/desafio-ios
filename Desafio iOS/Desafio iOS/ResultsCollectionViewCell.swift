//
//  ResultsCollectionViewCell.swift
//  Desafio iOS
//
//  Created by Alex Freitas on 23/11/21.
//

import UIKit

class ResultsCollectionViewCell: UICollectionViewCell {
    static let identifier = "ResultsCollectionViewCell"

    @IBOutlet weak var enterpriseImage: CustomImageView!
    @IBOutlet weak var enterpriseNameLabel: UILabel!
    @IBOutlet weak var enterpriseNameBgView: UIView!
    @IBOutlet weak var enterpriseBgImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }

    func setupView() {
        enterpriseNameBgView.addBottomCornerRadius()
        enterpriseNameBgView.addDropShadow()
        enterpriseBgImage.addTopCornerRadius()
        enterpriseImage.layer.cornerRadius = 16.0
    }

    func config(enterprise: Enterprise) {
        enterpriseNameLabel.text = enterprise.name

        if let imagePath = enterprise.photo {
            let imageUrl = Helpers.getImageURL(path: imagePath)
            enterpriseImage.downloadImage(from: imageUrl)
        }
    }
}

extension UIView {
    func addDropShadow(color: UIColor = .lightGray, opacity: Float = 0.5, offSet: CGSize = .zero, radius: CGFloat = 5) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
    }

    func addBottomCornerRadius(cornerRadius: CGFloat = 16.0) {
        layer.cornerRadius = cornerRadius
        layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }

    func addTopCornerRadius(cornerRadius: CGFloat = 16.0) {
        layer.cornerRadius = cornerRadius
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
}
