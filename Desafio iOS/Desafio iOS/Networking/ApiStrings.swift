//
//  ApiStrings.swift
//  Desafio iOS
//
//  Created by Alex Freitas on 24/11/21.
//

import Foundation

struct ApiStrings {
    static let host = "https://empresas.ioasys.com.br"
    static let version = "v1"

    static let routeSignIn = "users/auth/sign_in"
    static let routeEnterprises = "enterprises"
    static let routeSearch = "enterprises?name="
}
