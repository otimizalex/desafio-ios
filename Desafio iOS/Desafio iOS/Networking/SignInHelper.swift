//
//  SignInHelper.swift
//  Desafio iOS
//
//  Created by Alex Freitas on 28/11/21.
//

import UIKit

enum KeychainKeys: String {
    case username
    case password
}

struct SignInHelper {
    static var email: String? {
        return KeychainWrapper.standard.string(forKey: .username)
    }

    static var password: String? {
        return KeychainWrapper.standard.string(forKey: .password)
    }

    static func save(_ signInInformation: SignInInformation) {
        KeychainWrapper.standard.set(signInInformation.email, forKey: .username)
        KeychainWrapper.standard.set(signInInformation.password, forKey: .password)
    }

    static func clear() {
        _ = KeychainWrapper.standard.removeAllKeys()
    }
}

extension KeychainWrapper {
    func set(_ value: String, forKey key: KeychainKeys) {
        set(value, forKey: key.rawValue)
    }

    func string(forKey key: KeychainKeys) -> String? {
        string(forKey: key.rawValue)
    }
}
