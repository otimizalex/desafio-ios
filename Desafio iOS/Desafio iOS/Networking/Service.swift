//
//  Service.swift
//  Desafio iOS
//
//  Created by Alex Freitas on 23/11/21.
//

import Foundation

enum NetworkError: Error {
    case unexpectedError
    case decodingError
}

class Service {

    enum HTTPMethod: String {
        case get
        case post
    }

    enum Route: String {
        case signIn = "/users/auth/sign_in"
        case enterprises = "/enterprises"
        case search = "/enterprises?name="
    }

    static let shared = Service()

    private let defaultSession = URLSession(configuration: .default)
    private let baseURL = "\(ApiStrings.host)/api/\(ApiStrings.version)/"

    private var accessToken: String?
    private var client: String?
    private var uid: String?

    private func requestConfiguration(route: String, method: HTTPMethod = .get) -> URLRequest? {
        guard let url = URL(string: baseURL + route) else {
            return nil
        }

        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(accessToken, forHTTPHeaderField: "access-token")
        request.setValue(client, forHTTPHeaderField: "client")
        request.setValue(uid, forHTTPHeaderField: "uid")
        request.httpMethod = method.rawValue

        return request
    }

    private func requestConfiguration<T: Encodable>(route: String, method: HTTPMethod = .get, body: T) -> URLRequest? {
        var request = requestConfiguration(route: route, method: method)
        request?.httpBody = try? JSONEncoder().encode(body)
        return request
    }

    private func updateHeaders(from response: HTTPURLResponse) {
        accessToken = response.allHeaderFields["access-token"] as? String
        client = response.allHeaderFields["client"] as? String
        uid = response.allHeaderFields["uid"] as? String
    }

    func signIn(with information: SignInInformation, completion: @escaping (Result<Investor, NetworkError>) -> Void) {
        guard let request = requestConfiguration(route: ApiStrings.routeSignIn, method: .post, body: information) else { return }

        defaultSession.dataTask(with: request) { data, response, error in
            guard
                let data = data,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200
            else {
                SignInHelper.clear()
                completion(.failure(.unexpectedError))
                return
            }

            self.updateHeaders(from: response)
            SignInHelper.save(information)

            do {
                let investorResponse = try JSONDecoder().decode(InvestorResponse.self, from: data)
                completion(.success(investorResponse.investor))
            } catch {
                SignInHelper.clear()
                completion(.failure(.decodingError))
            }
        }.resume()
    }

    func getEnterprises(completion: @escaping (Result<EnterpriseListResponse, NetworkError>) -> Void) {
        guard let request = requestConfiguration(route: ApiStrings.routeEnterprises, method: .get) else { return }

        defaultSession.dataTask(with: request) { data, response, error in
            if error != nil {
                completion(.failure(.unexpectedError))
            } else if let data = data,
                      let response = response as? HTTPURLResponse,
                      response.statusCode == 200 {
                do {
                    let decoder = JSONDecoder()
                    let enterpriseList = try decoder.decode(EnterpriseListResponse.self, from: data)
                    completion(.success(enterpriseList))
                } catch {
                    completion(.failure(.decodingError))
                }
            }
        }.resume()
    }

    func search(by name: String, completion: @escaping (Result<EnterpriseListResponse, NetworkError>) -> Void) {
        let searchPath = ApiStrings.routeSearch + name
        guard let request = requestConfiguration(route: searchPath, method: .get) else { return }

        defaultSession.dataTask(with: request) { data, response, error in
            if error != nil {
                completion(.failure(.unexpectedError))
            } else if let data = data,
                      let response = response as? HTTPURLResponse,
                      response.statusCode == 200 {
                do {
                    let decoder = JSONDecoder()
                    let enterpriseList = try decoder.decode(EnterpriseListResponse.self, from: data)
                    completion(.success(enterpriseList))
                } catch {
                    completion(.failure(.decodingError))
                }
            }
        }.resume()
    }
}
/*
 https://empresas.ioasys.com.br/api/v1/users/auth/sign_in

 {{dev_host}}/api/{{api_version}}/users/auth/sign_in

 Para o login usamos padrões OAuth 2.0. Na resposta de sucesso do login a api retornará 3 custom headers
    (access-token, client, uid);
 Para ter acesso as demais APIS precisamos enviar esses 3 custom headers para a API autorizar a requisição;

 Servidor: https://empresas.ioasys.com.br/api
 Versão da API: v1
 Usuário de Teste: testeapple@ioasys.com.br
 Senha de Teste : 12341234


 request.addValue("eyJhbGcidQssw5c", forHTTPHeaderField: "token")


 {
     "enterprises": [
         {
             "id": 6,
             "email_enterprise": null,
             "facebook": null,
             "twitter": null,
             "linkedin": null,
             "phone": null,
             "own_enterprise": false,
             "enterprise_name": "Veuno Ltd.",
             "photo": "/uploads/enterprise/photo/6/240.jpeg",
             "description": "Problem identified: People have difficulties managing money and coordinating across multiple accounts.\n\nSolution: Bring all accounts in one place for easier management using a goal-based approach and third party service recommendations for efficient spending, everything inside a mobile app.\n\nOur goal is to help people get more value from their money.",
             "city": "London",
             "country": "UK",
             "value": 0,
             "share_price": 5000.0,
             "enterprise_type": {
                 "id": 2,
                 "enterprise_type_name": "Fintech"
             }
         },
         {
             "id": 9,
             "email_enterprise": null,
             "facebook": null,
             "twitter": null,
             "linkedin": null,
             "phone": null,
             "own_enterprise": false,
             "enterprise_name": "Invest In Crowd Ltd",
             "photo": "/uploads/enterprise/photo/9/240.jpeg",
             "description": "We have an equity only crowdfunding platform for property developers and investors.",
             "city": "London",
             "country": "UK",
             "value": 0,
             "share_price": 5000.0,
             "enterprise_type": {
                 "id": 2,
                 "enterprise_type_name": "Fintech"
             }
         },
         {
             "id": 11,
             "email_enterprise": null,
             "facebook": null,
             "twitter": null,
             "linkedin": null,
             "phone": null,
             "own_enterprise": false,
             "enterprise_name": "Investment Searcher",
             "photo": "/uploads/enterprise/photo/11/240.jpeg",
             "description": "An automated matching making service for investors from around the world and entrepreneurs looking for finance. The tech is built and the data is collected. The company will launch in the next two months. This is an investment in a company that finds its competition investment!",
             "city": "Cardiff",
             "country": "UK",
             "value": 0,
             "share_price": 5000.0,
             "enterprise_type": {
                 "id": 2,
                 "enterprise_type_name": "Fintech"
             }
         },
         {
             "id": 18,
             "email_enterprise": null,
             "facebook": null,
             "twitter": null,
             "linkedin": null,
             "phone": null,
             "own_enterprise": false,
             "enterprise_name": "Assetvault",
             "photo": "/uploads/enterprise/photo/18/240.jpeg",
             "description": "AssetVault provides digital legal and financial products to large Fiancial Institutions to help them get a deeper share of wallet from existing customers and acquire new customers",
             "city": "London",
             "country": "UK",
             "value": 0,
             "share_price": 5000.0,
             "enterprise_type": {
                 "id": 2,
                 "enterprise_type_name": "Fintech"
             }
         },
         {
             "id": 21,
             "email_enterprise": null,
             "facebook": null,
             "twitter": null,
             "linkedin": null,
             "phone": null,
             "own_enterprise": false,
             "enterprise_name": "Moniday Ltd",
             "photo": "/uploads/enterprise/photo/21/240.jpeg",
             "description": "Wealth Management for mass market within retail sector",
             "city": "London",
             "country": "UK",
             "value": 0,
             "share_price": 5000.0,
             "enterprise_type": {
                 "id": 2,
                 "enterprise_type_name": "Fintech"
             }
         },
         {
             "id": 24,
             "email_enterprise": null,
             "facebook": null,
             "twitter": null,
             "linkedin": null,
             "phone": null,
             "own_enterprise": false,
             "enterprise_name": "World Wide Generation",
             "photo": "/uploads/enterprise/photo/24/240.jpeg",
             "description": "World Wide Generation is creating a Distributed Ledger Ecosystem, G17Eco, to improve our world by validating and reporting on the impact of global social investment.\n\n\nG17Eco will  map and measure the financial and social impact of investments around the world against the UN’s 17 Sustainable Development Goals. It will capture independently validated data in a secure blockchain database to assist government, philanthropic, corporates, and individual investors in gaining confidence in their choice of direct investment and it is having maximum beneficial effect.\n",
             "city": "London",
             "country": "UK",
             "value": 0,
             "share_price": 5000.0,
             "enterprise_type": {
                 "id": 2,
                 "enterprise_type_name": "Fintech"
             }
         },
         {
             "id": 29,
             "email_enterprise": null,
             "facebook": null,
             "twitter": null,
             "linkedin": null,
             "phone": null,
             "own_enterprise": false,
             "enterprise_name": "DealGlobe",
             "photo": "/uploads/enterprise/photo/29/240.jpeg",
             "description": "DealGlobe is a technology driven advisory firm specialised on China cross-border M&A and capital raising. ",
             "city": "London",
             "country": "UK",
             "value": 0,
             "share_price": 5000.0,
             "enterprise_type": {
                 "id": 2,
                 "enterprise_type_name": "Fintech"
             }
         },
         {
             "id": 44,
             "email_enterprise": null,
             "facebook": null,
             "twitter": null,
             "linkedin": null,
             "phone": null,
             "own_enterprise": false,
             "enterprise_name": "Lendhaus",
             "photo": "/uploads/enterprise/photo/44/240.jpeg",
             "description": "Turbocharging the refinancing of commercial property.",
             "city": "London",
             "country": "UK",
             "value": 0,
             "share_price": 5000.0,
             "enterprise_type": {
                 "id": 2,
                 "enterprise_type_name": "Fintech"
             }
         }
     ]
 }

 */
