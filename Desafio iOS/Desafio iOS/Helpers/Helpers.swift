//
//  Helpers.swift
//  Desafio iOS
//
//  Created by Alex Freitas on 27/11/21.
//

import Foundation
import UIKit

struct Helpers {
    static func getImageURL(path: String) -> URL {
        let baseURL = ApiStrings.host
        let imageURL: URL = URL(string: baseURL + path)!
        return imageURL
    }
}
