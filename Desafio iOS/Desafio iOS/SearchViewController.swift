//
//  SearchViewController.swift
//  Desafio iOS
//
//  Created by Alex Freitas on 23/11/21.
//

import UIKit

class SearchViewController: UIViewController {

    private lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: resultsController)
        searchController.searchResultsUpdater = self
        return searchController
    }()

    private lazy var resultsController: ResultsCollectionViewController? = {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ResultsCollectionViewController") as? ResultsCollectionViewController
        viewController?.delegate = self
        return viewController
    }()

    private var selectedEnterprise: Enterprise?

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.searchController = searchController
    }

    private func searchEnterprise(name: String) {
        Service.shared.search(by: name) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case let .failure(error):
                    print(error)
                case let .success(response):
                    self?.resultsController?.enterprises = response.list
                }
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? EnterpriseViewController {
            destination.enterprise = selectedEnterprise
        }
    }
}

extension SearchViewController: ResultsCollectionDelegate {
    func didSelect(item: Enterprise) {
        selectedEnterprise = item
        performSegue(withIdentifier: "ShowEnterpriseDetails", sender: nil)
    }
}

extension SearchViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let input = searchController.searchBar.text else { return }
        searchEnterprise(name: input)
    }
}
