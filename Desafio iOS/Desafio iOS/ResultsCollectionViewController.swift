//
//  ResultsCollectionViewController.swift
//  Desafio iOS
//
//  Created by Alex Freitas on 28/11/21.
//

import UIKit

protocol ResultsCollectionDelegate {
    func didSelect(item: Enterprise)
}

class ResultsCollectionViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!

    var enterprises: [Enterprise] = [] {
        didSet {
            collectionView.reloadData()
        }
    }

    var delegate: ResultsCollectionDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib(nibName: ResultsCollectionViewCell.identifier, bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: ResultsCollectionViewCell.identifier)
        collectionView.delegate = self
    }
}

extension ResultsCollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (self.enterprises.count == 0) {
            self.collectionView.setEmptyMessage()
        } else {
            self.collectionView.restore()
        }

        return enterprises.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ResultsCollectionViewCell.identifier, for: indexPath) as? ResultsCollectionViewCell else {
            return UICollectionViewCell()
        }

        cell.config(enterprise: enterprises[indexPath.item])

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout else { return .zero }

        let spaces = flowLayout.minimumInteritemSpacing + flowLayout.sectionInset.left + flowLayout.sectionInset.right
        let size = (collectionView.frame.size.width - spaces) / 2.0
        return CGSize(width: size, height: size - 10)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didSelect(item: enterprises[indexPath.item])
    }
}

private extension UICollectionView {

    func setEmptyMessage() {
        self.backgroundView = Bundle.main.loadNibNamed("NotFoundView", owner: nil, options: nil)?.first as? UIView
    }

    func restore() {
        self.backgroundView = nil
    }
}
