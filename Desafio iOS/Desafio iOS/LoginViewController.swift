//
//  LoginViewController.swift
//  Desafio iOS
//
//  Created by Alex Freitas on 22/11/21.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet private weak var loginInputsView: UIView!
    @IBOutlet private weak var emailTextField: UITextField!
    @IBOutlet private weak var emailLabel: UILabel!
    @IBOutlet private weak var emailErrorLabel: UILabel!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var passwordLabel: UILabel!
    @IBOutlet private weak var loginButton: UIButton!

    @IBAction func showHidePassword(_ sender: Any) {
        passwordTextField.isSecureTextEntry.toggle()
    }

    private lazy var loadingViewController: LoadingViewController = {
        let loadingViewController = LoadingViewController()
        loadingViewController.modalPresentationStyle = .overCurrentContext
        loadingViewController.modalTransitionStyle = .crossDissolve
        return loadingViewController
    }()

    private var buttonEnabled = false

    override func viewDidLoad() {
        super.viewDidLoad()

        view.keyboardLayoutGuide.followsUndockedKeyboard = true
        view.keyboardLayoutGuide.topAnchor.constraint(equalTo: loginInputsView.bottomAnchor).isActive = true

        configureLoginButton()
        configureTextFields()

        if let email = SignInHelper.email,
           let password = SignInHelper.password {
            signIn(email: email, password: password)
        }
    }

    private func configureLoginButton() {
        loginButton.backgroundColor = AppColors.loginButtonInactive
        loginButton.layer.cornerRadius = 24.0
    }

    private func configureTextFields() {
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }

    func textFieldDidChangeSelection(_ textField: UITextField) {
        updateLabel(for: textField)
        verifyTextField(textField)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        updateLabel(for: textField)

        if textField == emailTextField, let email = textField.text {
            emailErrorLabel.isHidden = email.isEmpty || isValidEmail(email: email)
        }
    }

    private func updateLabel(for textField: UITextField) {
        let label = textField == emailTextField ? emailLabel : passwordLabel
        label?.isHidden = textField.text?.isEmpty == true
    }

    private func verifyTextField(_ textField: UITextField) {
        let isEmpty = emailTextField.text?.isEmpty == true || passwordTextField.text?.isEmpty == true

        loginButton.backgroundColor = isEmpty ? AppColors.loginButtonInactive : AppColors.loginButtonActive
        buttonEnabled = !isEmpty
    }

    private func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }

    private func trySignIn() {
        guard let email = emailTextField.text,
           let password = passwordTextField.text,
           !email.isEmpty, !password.isEmpty, buttonEnabled
        else {
            return
        }

        signIn(email: email, password: password)
    }

    private func signIn(email: String, password: String) {
        present(loadingViewController, animated: true)

        Service.shared.signIn(with: SignInInformation(email: email, password: password)) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(_):
                    self?.showMainScreen()
                case let .failure(error):
                    self?.loadingViewController.dismiss(animated: true)
                    print(error)
                }
            }
        }
    }

    @IBAction func didTapLogin() {
        trySignIn()
    }


    func showMainScreen() {
        guard
            let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
            let window = windowScene.windows.first
        else {
            return
        }
        window.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
        window.makeKeyAndVisible()
    }
}

